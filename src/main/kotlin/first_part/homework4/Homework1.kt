package homework4

fun main() {
    var evenCount = 0
    var oddCount = 0

    val myArray = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)
    for (count in myArray) {
        if (count % 2 == 0) {
            evenCount++
        } else {
            oddCount++
        }
    }
    println("Количество четных монет: $evenCount")
    println("Количество нечетных монет: $oddCount")
}
