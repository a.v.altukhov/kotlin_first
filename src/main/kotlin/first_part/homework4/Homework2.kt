package homework4

fun main() {
    var numberTwo = 0.0
    var numberThrees = 0.0
    var numberFours = 0.0
    var numberFives = 0.0

    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)

    for (count in marks) {
        when (count) {
            2 -> numberTwo++
            3 -> numberThrees++
            4 -> numberFours++
            5 -> numberFives++
        }
    }
    println("Отличников - ${calculateMarkPercent(numberFives, marks.size)}%")
    println("Хорошистов - ${calculateMarkPercent(numberFours, marks.size)}%")
    println("Троечников - ${calculateMarkPercent(numberThrees, marks.size)}%")
    println("Двоечников - ${calculateMarkPercent(numberTwo, marks.size)}%")
}

fun calculateMarkPercent(numberRatings: Double, numberStudents: Int): Double {
    return (numberRatings / numberStudents) * 100
}