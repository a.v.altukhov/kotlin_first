package homework5

fun main() {
    val digit = readLine()!!.toInt()
    val reversedDigit = flipNumber(digit)
    println(reversedDigit)
}

fun flipNumber(num: Int): Int {
    var _num = num
    var reversed = 0

    while (_num != 0) {
        val digit = _num % 10
        reversed = reversed * 10 + digit
        _num /= 10
    }
    return reversed
}