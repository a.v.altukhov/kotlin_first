package homework8

val userCart = mutableMapOf<String, Int>(
    "potato" to 2,
    "cereal" to 2,
    "milk" to 1,
    "sugar" to 3,
    "onion" to 1,
    "tomato" to 2,
    "cucumber" to 2,
    "bread" to 3
)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun main() {
    println("Количество овощей: ${calculatingVegetablesInBasket(userCart)}")
    println("Сумма товаров: ${amountPurchases(userCart)}")
}

fun amountPurchases(shoppingBasket: MutableMap<String, Int>): Double {
    var totalAmount = 0.0
    val discount: Double = 1 - discountValue
    for ((keyCart, valueCart) in shoppingBasket) {
        if ((prices.containsKey(keyCart)) && (discountSet.contains(keyCart))) {
            totalAmount += (valueCart * prices.get(keyCart)!!) * discount
            continue
        }
        if (prices.containsKey(keyCart)) {
            totalAmount += valueCart * prices.get(keyCart)!!
        }
    }
    return totalAmount
}

fun calculatingVegetablesInBasket(shoppingBasket: MutableMap<String, Int>): Int {
    var numberVegetables = 0

    vegetableSet.forEach {
        if (shoppingBasket.containsKey(it)) {
            numberVegetables += shoppingBasket.getValue(it)
        }
    }
    return numberVegetables
}
