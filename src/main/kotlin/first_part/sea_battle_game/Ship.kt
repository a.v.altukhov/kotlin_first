package sea_battle_game

class Ship(val shipType: ShipType)

enum class ShipType(val designationOnTheField: Char, val numberOfOccupiedCells: Int) {
    SINGLE_DECK('1', 1),
    DOUBLEDECKER('2', 2)
}