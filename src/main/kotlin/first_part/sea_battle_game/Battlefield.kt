package sea_battle_game

class Battlefield {

    private var counter = 0
    private var coordinateX: String = ""
    private var coordinateY: Int = 0

    private val battlefield = linkedMapOf<String, Array<Char>>(
        "A" to arrayOf(' ', ' ', ' ', ' '),
        "B" to arrayOf(' ', ' ', ' ', ' '),
        "C" to arrayOf(' ', ' ', ' ', ' '),
        "D" to arrayOf(' ', ' ', ' ', ' ')
    )

    val listKeyBattlefield = battlefield.keys.toList()

    fun drawingPlayingField() {
        for ((key, value) in battlefield) {
            for (count in battlefield[key]!!) {
                print("|$count|")
            }
            println()
        }
    }

    fun fieldFilling(x: String, y: Int, shipType: Int) {

        // сделать проверку на фуфло

        if ((y in 1..4) && (shipType == 1)) {
            battlefield[x]?.set(y - 1, '1')
        } else if ((y in 1..4) && (shipType == 2)) {
            battlefield[x]?.set(y - 1, '2')
        } else {
            println("Вы вышли за пределы игрового поля")
        }
    }

    // метод расчитывающий можно ли разместить корабль

    fun makeAShipPlacementAnalysis(x: String, y: Int, shipType: Int, orientation: Orientation): Boolean {
        counter = 0
        coordinateX = x
        coordinateY = y

        if (orientation == Orientation.HORIZONTALLY) {
            while (counter < shipType) {
                try {
                    if (battlefield[x]?.get(coordinateY - 1) == ' ') {
                        coordinateY++
                        counter++
                    } else return false
                } catch (e: ArrayIndexOutOfBoundsException) {
                    return false
                }
            }
        }

        if (orientation == Orientation.VERTICALLY) {
            while (counter < shipType) {
                try {
                    if (battlefield[coordinateX]?.get(coordinateY - 1) == ' ') {
                        coordinateX = listKeyBattlefield[listKeyBattlefield.indexOf(coordinateX) + 1]
                        counter++
                    } else return false
                } catch (e: IndexOutOfBoundsException) {
                    return false
                }
            }
        }
        return true
    }

    fun placeShip(x: String, y: Int, ship: Ship, orientation: Orientation) {
        if (orientation == Orientation.HORIZONTALLY) {
            setHorizontalPositioningOfTheShip(x, y, ship)
        } else {
            setVerticalPositioningOfTheShip(x, y, ship)
        }
    }

    private fun setHorizontalPositioningOfTheShip(x: String, y: Int, ship: Ship) {
        if (makeAShipPlacementAnalysis(x, y, ship.shipType.numberOfOccupiedCells, Orientation.HORIZONTALLY)) {
            counter = 0
            coordinateY = y

            while (counter < ship.shipType.numberOfOccupiedCells) {
                battlefield[x]?.set(coordinateY - 1, ship.shipType.designationOnTheField)
                coordinateY++
                counter++
            }
        } else println("Выберете другие координаты коробля")
    }

    private fun setVerticalPositioningOfTheShip(x: String, y: Int, ship: Ship) {
        if (makeAShipPlacementAnalysis(x, y, ship.shipType.numberOfOccupiedCells, Orientation.VERTICALLY)) {
            counter = 0
            coordinateX = x
            coordinateY = y

            while (counter < ship.shipType.numberOfOccupiedCells) {
                battlefield[coordinateX]?.set(coordinateY - 1, ship.shipType.designationOnTheField)
                coordinateX = listKeyBattlefield[listKeyBattlefield.indexOf(coordinateX) + 1]
                counter++
            }
        } else println("Выберете другие координаты коробля")
    }

    //метод проверки входит ли буква в массиве букв

}

enum class Orientation {
    HORIZONTALLY,
    VERTICALLY,
    NO_DIRECTION
}