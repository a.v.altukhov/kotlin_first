package sea_battle_game

class Game {

    private var activeGame: Boolean = false

    private var coordinateX: String = ""
    private var coordinateY: Int = 0
    private var orientation: Orientation = Orientation.NO_DIRECTION

    // Создание игроков
    fun playerCreation() {
        val battlefieldFirstPlayer: Battlefield = Battlefield()
        println("Введите имя первого игрока: ")
        val firstPlayer = Player(readlnOrNull() ?: "User", battlefieldFirstPlayer)

        val battlefieldSecondPlayer: Battlefield = Battlefield()
        println("Введите имя второго игрока: ")
        val secondPlayer = Player(readlnOrNull() ?: "User", battlefieldSecondPlayer)

        println("Игрок ${firstPlayer.name}, разместите корабли.")
        buildingShips(firstPlayer)

        println("Игрок ${secondPlayer.name}, разместите корабли.")
        buildingShips(secondPlayer)




    }

    private fun startGame() {
        activeGame = true
    }

    private fun finishGame() {
        activeGame = false
    }

    // создание кораблей
    fun buildingShips(player: Player) {

        val firstShip: Ship = Ship(ShipType.SINGLE_DECK)
        val secondShip: Ship = Ship(ShipType.SINGLE_DECK)
        val thirdShip: Ship = Ship(ShipType.DOUBLEDECKER)

        player.listShip.addAll(listOf(firstShip, secondShip, thirdShip))


        println("Укажите координаты первого однопалубного корабля: ")
        coordinateX = inputCoordinateX(player)
        if (coordinateX == "") return

        coordinateY = inputCoordinateY()
        if (coordinateY == 0) return

        orientation = determinationOfTheDirectionOfMovement()
        if (orientation == Orientation.NO_DIRECTION) return
        player.battlefield.placeShip(coordinateX, coordinateY, firstShip, orientation)

        println("Укажите координаты второго однопалубного корабля: ")
        coordinateX = inputCoordinateX(player)
        if (coordinateX == "") return

        coordinateY = inputCoordinateY()
        if (coordinateY == 0) return

        orientation = determinationOfTheDirectionOfMovement()
        if (orientation == Orientation.NO_DIRECTION) return
        player.battlefield.placeShip(coordinateX, coordinateY, firstShip, orientation)

        println("Укажите координаты двухпалубного корабля: ")
        coordinateX = inputCoordinateX(player)
        if (coordinateX == "") return

        coordinateY = inputCoordinateY()
        if (coordinateY == 0) return

        orientation = determinationOfTheDirectionOfMovement()
        if (orientation == Orientation.NO_DIRECTION) return
        player.battlefield.placeShip(coordinateX, coordinateY, firstShip, orientation)
    }

    private fun inputCoordinateX(player: Player): String {
        var count = 0
        var х: String = ""

        print("Введите букву из координаты X: ")
        х = readLine()!!
        while (!player.battlefield.listKeyBattlefield.contains(х)) {
            println("Введите букву из координаты корректно : ")
            х = readLine()!!
            count++
            if (count == 3) {
                println("Вы ввели не верно координаты не верно 4 раза. Игра окончена.")
                return ""
            }
        }
        return х
    }

    private fun inputCoordinateY(): Int {
        var count = 0

        print("Введите число из координаты У: ")
        var y: Int = readLine()!!.toInt()

        if (y in 1..4) {
            return y
        } else {
            while (y !in 1..4) {
                println("Введите число из координаты У с 1 по 4: ")
                y = readLine()!!.toInt()
                count++
                if (count == 3) {
                    println("Вы ввели не верно координаты не верно 4 раза. Игра окончена.")
                    return 0
                }
            }
            return y
        }
    }

    private fun determinationOfTheDirectionOfMovement(): Orientation {
        var count = 3
        var orientationType: Int

        println("Определите направление расположение корабля")
        print("Введите 1 по вертикали или 2 по горизонтали: ")
        orientationType = readLine()!!.toInt()
        when (orientationType) {
            1 -> return Orientation.VERTICALLY
            2 -> return Orientation.HORIZONTALLY
            else -> {
                while (orientationType !in 1..2) {
                    print("Введите 1 по вертикали или 2 по горизонтали у вас $count попытки ")
                    orientationType = readLine()!!.toInt()
                    count--
                    if (count == 0) {
                        println("Вы ввели не верно 4 раза. Игра окончена.")
                        return Orientation.NO_DIRECTION
                    }
                }
            }
        }
        return Orientation.NO_DIRECTION
    }


    // булевое поле (функция) определяющее окончание игры
    // fun передача хода
    // fun выстрела (хода)
}

