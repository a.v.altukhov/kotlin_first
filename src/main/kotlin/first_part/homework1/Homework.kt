package homework1

fun main() {
    val whiskey: Byte = 50
    val pinaColade: Short = 200
    val lemonade: Int = 18500
    val fresh: Long = 3000000000
    val cola: Float = 0.5F
    val ale: Double = 0.666666667
    val string: String = "Что-то авторское!"

    println("Заказ - \'$whiskey мл виски\' готов!")
    println("Заказ - \'$pinaColade мл пина колады\' готов!")
    println("Заказ - \'$lemonade мл лимонада\' готов!")
    println("Заказ - \'$fresh капель фреша\' готов!")
    println("Заказ - \'$cola литра колы\' готов!")
    println("Заказ - \'$ale литра эля\' готов!")
    println("Заказ - \'$string\' готов!")
}
