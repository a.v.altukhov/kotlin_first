package homework6

abstract class Animal(val name: String, var height: Double, var weight: Double) {
    abstract val foodPreferences: Array<String>
    var satiety: Int = 0

    fun eat(food: String) {
        if (foodPreferences.contains(food)) satiety++
    }
}

class Lion(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences: Array<String> = arrayOf("Мясо", "Рыба", "Молоко")
}


class Tiger(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences = arrayOf("Мясо", "Корова", "Кабан")
}

class Hippo(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences = arrayOf("Трава", "Газель", "Корова")
}

class Wolf(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences = arrayOf("Заяц", "Лось", "Корова")
}

class Giraffe(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences = arrayOf("Листья", "Семена", "Фрукты")
}

class Elephant(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences = arrayOf("Трава", "Сено", "Яблоки", "Конфеты")
}

class Chimpanzee(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences = arrayOf("Стебли", "Листья", "Плоды", "Коренья")
}

class Gorilla(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val foodPreferences = arrayOf("Стебли", "Листья", "Плоды", "Коренья")
}

fun massAnimalFeeding(arrayAnimals: Array<Animal>, arrayFood: Array<String>) {
    for (animal in arrayAnimals) {
        for (food in arrayFood) {
            animal.eat(food)
        }
    }
}

fun main() {
    val simba = Lion("Симба", 0.75, 90.0)
    val shark = Tiger("Шакран", 0.75, 180.0)
    val motoMoto = Hippo("Мото-Мото", 1.90, 280.0)
    val whiteWolf = Wolf("Ауф", 1.45, 130.0)
    val melman = Giraffe("Мелман", 5.55, 900.0)
    val dino = Elephant("Динго", 3.75, 10_000.0)
    val byba = Chimpanzee("Буба", 1.60, 80.0)
    val gogo = Gorilla("Гого", 1.65, 250.0)

    val arrayAnimals = arrayOf(simba, shark, motoMoto, whiteWolf, melman, dino, byba, gogo)
    val arrayFoodPreferences = arrayOf("Мясо", "Рыба", "Мясо", "Конина", "Курица", "Мясо", "Плоды")

    massAnimalFeeding(arrayAnimals, arrayFoodPreferences)
    println(simba.satiety)

}