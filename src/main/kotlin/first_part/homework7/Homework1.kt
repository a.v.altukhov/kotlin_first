package homework7

fun main() {
    val user = userCreation("Alex", "1234567890", "1234567890")
    println(user.password)
}

fun userCreation(login: String, password: String, passwordConfirmation: String): User {
    if (password != passwordConfirmation) throw WrongPasswordException("Пароли не равны")
    if (login.length > 20) {
        throw WrongLoginException("Логин не соответствует условиям")
    }

    if (password.length < 10) {
        throw WrongPasswordException("Пароль не соответствует условиям")
    }
    return User(login, password)
}

class User(val login: String, val password: String)

class WrongLoginException(message: String) : Exception(message)

class WrongPasswordException(message: String) : Exception(message)
